## 0.1.1 - 2023-06-01

* Bug fixes in logs
* updating to new go_router notation

## 0.1.0 - 2023-06-01

* Moving to Dart 3.0 and Flutter 3.10
* Changed event and state classes from abstract to sealed classes

## 0.0.5 - 2023-04-04

* Replacing FirstEvent by InitEvent

## 0.0.4 - 2023-04-02

* Fixing view bloc template and adding const constructor

## 0.0.3 - 2023-03-30

* Adding generation of router with package go_router

## 0.0.2 - 2023-03-29

* Fixing generating error


## 0.0.1 - 2023-03-28

* First version
