# What is flutter_util_generator

Flutter_util_generator is a file generator for flutter that will allow you tu easily generate files.

Install the package by running this command in your terminal :

```shell
flutter pub add flutter_util_generator
```

or by adding this to your pubspec.yaml file :

```yaml
dev_dependencies:
  flutter_util_generator: last_version
```

# How to use
Chose between the different generators and run the command in your terminal.

## Bloc generator
One feature is to generate a bloc file with the bloc pattern in the following structure :

```
example_bloc
├── bloc
│   ├── bloc.dart
│   ├── bloc_event.dart
│   └── bloc_state.dart
└── view
    └── view.dart
``` 

To generate the files, run this command in your terminal :

```
flutter pub run flutter_util_generator:generate_bloc --name blocName --path path/to/your/bloc
```

or

```
flutter pub run flutter_util_generator:generate_bloc -n blocName -p path/to/your/bloc
```

## Router generator
Generate all the files needed to use and set up the package go_router that uses a ShellRoute to surround most of the routes of your app.
The generated code uses the package [go_router](https://pub.dev/packages/go_router) to generate the routes.

You can choose between two types of ShellRoutes :
- Navigation Bar
- Navigation Rail

```
navigation
├── app_router.dart
├── routes.dart
├── scaffold_with_nav_bar.dart
└── scaffold_with_nav_bar_destination.dart
```

``` 
navigation
├── app_router.dart
├── routes.dart
├── scaffold_with_navigation_rail.dart
└── scaffold_with_navigation_rail_destination.dart
```

To generate the files, run this command in your terminal :

```
flutter pub run flutter_util_generator:generate_router --type rail --path path/to/your/navigation
flutter pub run flutter_util_generator:generate_router --type navbar --path path/to/your/navigation
```

Once the files are generated, you can change the routes and the tabs of the rail or nav bar to your needs. 
You also need to set custom routes in the app_router.dart file and in the routes.dart file.

To use the new router in your app, you need to make changes in your `MyApp` widget:

Replace 
```dart
return MaterialApp(
  [...]
)
```
with 
```dart
return MaterialApp.router(
  routerConfig: AppRouter.getInstance().router,
  [...]
)
```

and you're good to go !