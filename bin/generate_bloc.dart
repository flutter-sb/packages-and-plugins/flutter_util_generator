import 'dart:io';

import 'package:args/args.dart';
import 'package:flutter_util_generator/flutter_util_generator.dart';

void main(List<String> args) {
  print('Generating BLoC...');
  final parser = ArgParser()
    ..addOption('name', abbr: 'n', help: 'Name of the BLoC')
    ..addOption('path', abbr: 'p', help: 'Path to the destination folder');

  final argsResults = parser.parse(args);

  _checkArgs(argsResults, parser);

  generateBloc(argsResults['name'], argsResults['path']);
}

/// Check if the arguments are valid
void _checkArgs(argsResults, parser) {
  /// Check if the arguments are valid
  if (argsResults['name'] == null || argsResults['path'] == null) {
    print('Please provide both name and path arguments.\n');
    print(parser.usage);
    exit(1);
  }

  /// Check if the name contains slash
  if (argsResults['name'].contains('/')) {
    print(
        'Please provide a name without slash for the bloc to generate. First argument is the bloc name, second argument is the path.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains space
  if (argsResults['name'].contains(' ')) {
    print('Please provide a name without space for the bloc to generate.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains dash
  if (argsResults['name'].contains('-')) {
    print('Please provide a name without dash for the bloc to generate.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains space
  if (argsResults['path'].contains(' ')) {
    print('Please provide a path without space for the bloc to generate.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains dash
  if (argsResults['path'].contains('-')) {
    print('Please provide a path without dash for the bloc to generate.');
    print(parser.usage);
    exit(1);
  }
}
