import 'dart:io';

import 'package:args/args.dart';
import 'package:flutter_util_generator/flutter_util_generator.dart';

void main(List<String> args) {
  print('Generating Router...');
  final parser = ArgParser()
    ..addOption('path', abbr: 'p', help: 'Path to the destination folder')
    ..addOption('type',
        abbr: 't',
        help:
            'Type of the router, either rail (for a navigation rail) or navBar (for a bottom navigation bar)');

  final argsResults = parser.parse(args);

  _checkArgs(argsResults, parser);

  generateRouter(argsResults['type'].toString().toLowerCase(), argsResults['path'].toString().toLowerCase());
}

/// Check if the arguments are valid
void _checkArgs(argsResults, parser) {
  /// Check if the arguments are valid
  if (argsResults['type'] == null || argsResults['path'] == null) {
    print('Please provide both type and path arguments.\n');
    print(parser.usage);
    exit(1);
  }

  /// Check if the type is valid
  if (argsResults['type'].toString().toLowerCase() != 'rail' &&
      argsResults['type'].toString().toLowerCase() != 'navbar') {
    print(
        'Please provide a type of either rail or navbar for the router to generate. First argument is the router type, second argument is the path.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains space
  if (argsResults['path'].contains(' ')) {
    print('Please provide a path without space for the navigation to generate.');
    print(parser.usage);
    exit(1);
  }

  /// Check if the path contains dash
  if (argsResults['path'].contains('-')) {
    print('Please provide a path without dash for the navigation to generate.');
    print(parser.usage);
    exit(1);
  }
}
