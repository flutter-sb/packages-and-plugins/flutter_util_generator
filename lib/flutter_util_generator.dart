library flutter_util_generator;

import 'dart:io';
import 'dart:isolate';

import 'package:recase/recase.dart';
import 'package:path/path.dart' as p;

part 'bloc_generator/generate.dart';
part 'router_generator/generate.dart';
