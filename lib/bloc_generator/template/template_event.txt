part of 'template_bloc.dart';

sealed class TemplateEvent {
  const TemplateEvent();
}

class TemplateInitEvent extends TemplateEvent {}
