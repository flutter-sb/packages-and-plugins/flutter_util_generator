part of flutter_util_generator;

/// Generate BLoC
void generateBloc(String name, String path) async {
  /// Create all the variables
  final folderName = ReCase(name).snakeCase;
  final blocFolderPath = p.join(path, folderName);
  final viewFolderPath = p.join(blocFolderPath, 'view');
  final blocFolderPathInside = p.join(blocFolderPath, 'bloc');
  final String templateFolderPath = (await Isolate.resolvePackageUri(
          Uri.parse('package:flutter_util_generator/bloc_generator/template')))!
      .path;

  /// Create the folders
  Directory(viewFolderPath).createSync(recursive: true);
  Directory(blocFolderPathInside).createSync(recursive: true);

  print('Creating folders: $viewFolderPath, $blocFolderPathInside');
  print(
      'Creating files: ${folderName}_view.dart, ${folderName}_bloc.dart, ${folderName}_event.dart, ${folderName}_state.dart');

  /// Create the files
  final templateViewFilePath = p.join(templateFolderPath, 'template_view.txt');
  final generatedViewFilePath =
      p.join(viewFolderPath, '${folderName}_view.dart');

  final templateBlocFilePath = p.join(templateFolderPath, 'template_bloc.txt');
  final generatedBlocFilePath =
      p.join(blocFolderPathInside, '${folderName}_bloc.dart');

  final templateEventFilePath =
      p.join(templateFolderPath, 'template_event.txt');
  final generatedEventFilePath =
      p.join(blocFolderPathInside, '${folderName}_event.dart');

  final templateStateFilePath =
      p.join(templateFolderPath, 'template_state.txt');
  final generatedStateFilePath =
      p.join(blocFolderPathInside, '${folderName}_state.dart');

  print('Reading template files...');

  /// Read the template files
  final templateViewFileContent = File(templateViewFilePath).readAsStringSync();
  final templateBlocFileContent = File(templateBlocFilePath).readAsStringSync();
  final templateEventFileContent =
      File(templateEventFilePath).readAsStringSync();
  final templateStateFileContent =
      File(templateStateFilePath).readAsStringSync();

  print('Generating BLoC files...');

  /// Replace the template with the name
  final generatedViewFileContent = templateViewFileContent
      .replaceAll('Template', ReCase(name).pascalCase)
      .replaceAll('template', folderName);

  final generatedBlocFileContent = templateBlocFileContent
      .replaceAll('Template', ReCase(name).pascalCase)
      .replaceAll('template', folderName);

  final generatedEventFileContent = templateEventFileContent
      .replaceAll('Template', ReCase(name).pascalCase)
      .replaceAll('template', folderName);

  final generatedStateFileContent = templateStateFileContent
      .replaceAll('Template', ReCase(name).pascalCase)
      .replaceAll('template', folderName);

  print('Writing BLoC files...');

  /// Write the generated files
  File(generatedViewFilePath).writeAsStringSync(generatedViewFileContent);
  File(generatedBlocFilePath).writeAsStringSync(generatedBlocFileContent);
  File(generatedEventFilePath).writeAsStringSync(generatedEventFileContent);
  File(generatedStateFilePath).writeAsStringSync(generatedStateFileContent);

  print('Generated BLoC files in $blocFolderPath');
}
