part of flutter_util_generator;

/// Generate BLoC
void generateRouter(String type, String path) async {
  /// Create all the variables
  const folderName = "navigation";
  final folderPath = p.join(path, folderName);
  late final String templateFolderPath;

  late final String scaffoldFileName;
  late final String scaffoldDestFileName;
  final String appRouterFileName = "app_router";
  final String routesFileName = "routes";

  if (type == "navbar") {
    scaffoldFileName = "scaffold_with_nav_bar";
    scaffoldDestFileName = "scaffold_with_nav_bar_destination";
    templateFolderPath = (await Isolate.resolvePackageUri(Uri.parse(
            'package:flutter_util_generator/router_generator/template_nav_bar')))!
        .path;
  } else {
    scaffoldFileName = "scaffold_with_navigation_rail";
    scaffoldDestFileName = "scaffold_with_navigation_rail_destination";
    templateFolderPath = (await Isolate.resolvePackageUri(Uri.parse(
            'package:flutter_util_generator/router_generator/template_navigation_rail')))!
        .path;
  }

  /// Create the folder
  print('Creating folders: $folderPath');
  Directory(folderPath).createSync(recursive: true);

  print('Creating files');

  /// Create the files
  final appRouterPath = p.join(templateFolderPath, '$appRouterFileName.txt');
  final generatedAppRouterPath = p.join(folderPath, '$appRouterFileName.dart');

  final routesPath = p.join(templateFolderPath, '$routesFileName.txt');
  final generatedRoutesPath = p.join(folderPath, '$routesFileName.dart');

  final scaffoldPath = p.join(templateFolderPath, '$scaffoldFileName.txt');
  final generatedscaffoldPath = p.join(folderPath, '$scaffoldFileName.dart');

  final scaffoldDestPath =
      p.join(templateFolderPath, '$scaffoldDestFileName.txt');
  final generatedScaffoldDestPath =
      p.join(folderPath, '$scaffoldDestFileName.dart');

  print('Reading template files...');

  /// Read the template files
  final templateAppRouterFileContent = File(appRouterPath).readAsStringSync();
  final templateRoutesFileContent = File(routesPath).readAsStringSync();
  final templateScaffoldFileContent = File(scaffoldPath).readAsStringSync();
  final templateScaffoldDestFileContent =
      File(scaffoldDestPath).readAsStringSync();

  print('Generating router files...');

  /// Replace the template with the name
  final generatedAppRouterFileContent = templateAppRouterFileContent;
  final generatedRoutesFileContent = templateRoutesFileContent;
  final generatedScaffoldFileContent = templateScaffoldFileContent;
  final generatedScaffoldDestFileContent = templateScaffoldDestFileContent;

  print('Writing router files...');

  /// Write the generated files
  File(generatedAppRouterPath).writeAsStringSync(generatedAppRouterFileContent);
  File(generatedRoutesPath).writeAsStringSync(generatedRoutesFileContent);
  File(generatedscaffoldPath).writeAsStringSync(generatedScaffoldFileContent);
  File(generatedScaffoldDestPath)
      .writeAsStringSync(generatedScaffoldDestFileContent);

  print('Generated router files in $folderPath');
}
